from django.shortcuts import render


def error_404(request, exception):
    data = {}
    return render(request,
                  template_name='404.html',
                  context=data)


# def error_500(exception):
#     data = {}
#     return render(template_name='500.html',
#                   context=data)


def error_403(request, exception):
    data = {}
    return render(request,
                  template_name='403.html',
                  context=data)


def error_400(request, exception):
    data = {}
    return render(request,
                  template_name='400.html',
                  context=data)

