from django.db import models
from PIL import Image

from accounts.models import User

from django.utils import timezone
from django.urls import reverse
from decimal import *
from phonenumber_field.modelfields import PhoneNumberField


class Category(models.Model):
    # class CATEGORY_CHOICES(models.IntegerChoices):
    #     PIGMENTS = 0, "Pigments"
    #     CARTRIDGES = 1, "Cartridges"
    #     CAPS = 2, "Caps"
    #     ANASTESIA = 3, "Anastesia"

    # title = models.PositiveIntegerField(choices=CATEGORY_CHOICES.choices, verbose_name='Название категории')
    title = models.CharField(max_length=155, verbose_name='Название категории')
    slug = models.SlugField(unique=True)
    image = models.ImageField(null=True, blank=True, verbose_name='Изображение', upload_to='covers')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('shop:category_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        if self.image:
            img = Image.open(self.image.path)

            if img.height > 600 or img.width > 600:
                output_size = (600, 600)
                img.thumbnail(output_size)
                img.save(self.image.path)


class Brand(models.Model):
    category = models.ForeignKey(Category, verbose_name='Категория', related_name="brands", on_delete=models.CASCADE)
    title = models.CharField(max_length=155, verbose_name='Название')
    slug = models.SlugField(unique=True)
    image = models.ImageField(null=True, blank=True, verbose_name='Изображение', upload_to='covers')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('shop:brand_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        super(Brand, self).save(*args, **kwargs)

        if self.image:
            img = Image.open(self.image.path)

            if img.height > 600 or img.width > 600:
                output_size = (600, 600)
                img.thumbnail(output_size)
                img.save(self.image.path)


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='Категория', related_name='category_products',
                                 on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Производитель', related_name="products", on_delete=models.CASCADE)
    name = models.CharField(max_length=155, verbose_name='Наименование')
    slug = models.SlugField(unique=True)
    image = models.ImageField(null=True, blank=True, verbose_name='Изображение', upload_to='covers')
    description = models.TextField(max_length=1024, verbose_name='Описание', null=True, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Цена', null=True)
    count = models.IntegerField(verbose_name='Количетво', blank=True, null=True)
    discount = models.IntegerField(verbose_name="Скидка", default=0)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Стоимость со скидкой')

    def full_name(self):
        return f'{self.brand.title} {self.name}'

    def _get_final_price(self):
        if self.discount:
            self.final_price = Decimal(self.price - (self.price / 100) * self.discount).quantize(Decimal('1.11'),
                                                                                                 rounding=ROUND_UP)
        else:
            self.final_price = self.price
        return self.final_price

    def get_absolute_url(self):
        return reverse('shop:product_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.brand.title} {self.name}'

    def save(self, *args, **kwargs):
        self.final_price = self._get_final_price()
        super(Product, self).save(*args, **kwargs)

        if self.image:
            img = Image.open(self.image.path)

            if img.height > 600 or img.width > 600:
                output_size = (600, 600)
                img.thumbnail(output_size)
                img.save(self.image.path)


class CartProduct(models.Model):
    buyer = models.ForeignKey(User, verbose_name='Покупатель', related_name='cartproduct_buyer',
                              on_delete=models.CASCADE, null=True, default=3)
    cart = models.ForeignKey('Cart', verbose_name='Корзина', related_name='cartproduct_cart', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name="Товар", related_name='cartproduct_product',
                                on_delete=models.CASCADE)
    qty = models.PositiveIntegerField(default=1)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='Общая цена', null=True)

    def __str__(self):
        return f'Cart product: {self.product.brand.title} {self.product.name}'

    def save(self, *args, **kwargs):
        self.final_price = self.qty * self.product.final_price
        super().save(*args, **kwargs)


class Cart(models.Model):
    owner = models.ForeignKey(User, verbose_name='Владелец', related_name='cart_owner', on_delete=models.CASCADE,
                              default=3)
    products = models.ManyToManyField(CartProduct, blank=True, related_name='cart_products')
    total_products = models.PositiveIntegerField(default=0)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Общая цена', default=0, null=True)
    in_order = models.BooleanField(default=False)
    for_anonymous_user = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        self.total_products = CartProduct.objects.filter(cart_id=self.id).count()
        self.final_price = sum(obj.final_price for obj in CartProduct.objects.filter(cart_id=self.id))
        super().save(*args, **kwargs)


class Order(models.Model):
    STATUS_NEW = 'new'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_READY = 'is_ready'
    STATUS_COMPLETED = 'completed'

    BUYING_TYPE_SELF = 'self'
    BUYING_TYPE_DELIVERY = 'delivery'

    STATUS_CHOICES = (
        (STATUS_NEW, 'Новый заказ'),
        (STATUS_IN_PROGRESS, 'Заказ в обработке'),
        (STATUS_READY, 'Заказ готов'),
        (STATUS_COMPLETED, 'Заказ выполнен')
    )

    BUYING_TYPE_CHOICES = (
        (BUYING_TYPE_SELF, 'Самовывоз'),
        (BUYING_TYPE_DELIVERY, 'Доставка')
    )

    customer = models.ForeignKey(User, verbose_name='Покупатель', related_name='related_orders',
                                 on_delete=models.CASCADE, null=True, default=3)
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255, verbose_name='Фамилия')
    phone = PhoneNumberField(verbose_name='Номер телефона',  region="UA")
    cart = models.ForeignKey(Cart, verbose_name='Корзина', on_delete=models.CASCADE, null=True)
    address = models.CharField(max_length=1024, verbose_name='Адрес', null=True, blank=True)
    status = models.CharField(
        max_length=100,
        verbose_name='Статус заказ',
        choices=STATUS_CHOICES,
        default=STATUS_NEW
    )
    buying_type = models.CharField(
        max_length=100,
        verbose_name='Тип заказа',
        choices=BUYING_TYPE_CHOICES,
        default=BUYING_TYPE_SELF
    )
    comment = models.TextField(verbose_name='Комментарий к заказу', null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата создания заказа')
    order_date = models.DateField(verbose_name='Дата получения заказа', auto_now=True, null=True)

    def get_final_price(self):
        final_price = self.cart.final_price
        return final_price

    def __str__(self):
        return str(self.id)


        