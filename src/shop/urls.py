from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from django.conf.urls.static import static

from shop.views import BaseShopView, CategoryDetailView, CartView, AddToCartView, DeleteFromCartView, ChangeQTYView, \
    ProductDetailView, CheckoutView, MakeOrderView, BrandDetailView

app_name = 'shop'

urlpatterns = [

    path('', BaseShopView.as_view(), name='mainpage'),
    path('category/<str:slug>/', CategoryDetailView.as_view(), name='category_detail'),
    path('brand/<str:slug>/', BrandDetailView.as_view(), name='brand_detail'),

    path('products/<str:slug>/', ProductDetailView.as_view(), name='product_detail'),

    path('cart/', CartView.as_view(), name='cart'),
    path('add-to-cart/<str:slug>/', AddToCartView.as_view(), name='add_to_cart'),
    path('remove-from-cart/<str:slug>/', DeleteFromCartView.as_view(), name='delete_from_cart'),
    path('change-qty/<str:slug>/', ChangeQTYView.as_view(), name='change_qty'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('make-order/', MakeOrderView.as_view(), name='make_order')
]