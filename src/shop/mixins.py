from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin


from shop.models import Cart, Category


class CartMixin(View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            user = request.user
            cart = Cart.objects.filter(owner=user, in_order=False).first()
            if not cart:
                cart = Cart.objects.create(owner=user)
        else:
            cart = Cart.objects.filter(for_anonymous_user=True).first()
            if not cart:
                cart = Cart.objects.create(for_anonymous_user=True)
        self.cart = cart
        return super().dispatch(request, *args, **kwargs)
