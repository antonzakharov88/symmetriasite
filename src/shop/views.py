from django.shortcuts import render
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt


from django.views.generic import DetailView, View
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.http import HttpResponseRedirect

from accounts.models import User
from shop.forms import OrderForm
from shop.mixins import CartMixin
from shop.models import Category, Product, Brand, CartProduct
from shop.utils import recalc_cart


class BaseShopView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.all()
        products = Product.objects.all
        context = {
            'categories': categories,
            'products': products,
            'cart': self.cart
        }
        return render(request, 'baseshop.html', context)


class CategoryDetailView(CartMixin, DetailView):

    model = Category
    context_object_name = 'category'
    template_name = 'category_detail.html'
    slug_url_kwarg = 'slug'

    def get_object(self):
        slug = self.kwargs.get('slug')
        return self.model.objects.get(slug=slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        context['brand_list'] = Brand.objects.filter(
            category=self.get_object()
        )
        context['categories'] = Category.objects.all()
        context['products'] = Product.objects.filter(category=self.get_object())
        context['cart'] = self.cart
        return context

    def get_queryset(self):
        return Brand.objects.filter(
            category=self.get_object())


class BrandDetailView(CartMixin, DetailView):

    model = Brand
    context_object_name = 'brand'
    template_name = 'brand_detail.html'
    slug_url_kwarg = 'slug'

    def get_object(self):
        slug = self.kwargs.get('slug')
        return self.model.objects.get(slug=slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = Brand.objects.filter(category=self.object.category_id)
        context['categories'] = Category.objects.all()

        context['products'] = Product.objects.filter(brand=self.get_object())
        context['cart'] = self.cart
        return context

    def get_queryset(self):
        return Product.objects.filter(
            brand=self.get_object())


class ProductDetailView(CartMixin, DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'product_detail.html'
    slug_url_kwarg = 'slug'

    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        return self.model.objects.get(slug=slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['cart'] = self.cart
        return context


class AddToCartView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        product_slug = kwargs.get('slug')
        product = Product.objects.get(slug=product_slug)
        cart_product, created = CartProduct.objects.get_or_create(
            buyer=self.cart.owner, cart=self.cart, product=product
        )
        if created:
            self.cart.products.add(cart_product)
        recalc_cart(self.cart)
        messages.add_message(request, messages.INFO, "Товар добавлен")
        return HttpResponseRedirect('/shop/')


class DeleteFromCartView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        product_slug = kwargs.get('slug')
        product = Product.objects.get(slug=product_slug)
        cart_product = CartProduct.objects.get(
            buyer=self.cart.owner, cart=self.cart, product=product
        )
        self.cart.products.remove(cart_product)
        cart_product.delete()
        recalc_cart(self.cart)
        messages.add_message(request, messages.INFO, "Товар удален")
        return HttpResponseRedirect('/shop/cart/')


class ChangeQTYView(CartMixin, View):

    def post(self, request, *args, **kwargs):
        product_slug = kwargs.get('slug')
        product = Product.objects.get(slug=product_slug)
        cart_product = CartProduct.objects.get(
            buyer=self.cart.owner, cart=self.cart, product=product
        )
        qty = int(request.POST.get('qty'))
        cart_product.qty = qty
        cart_product.save()
        recalc_cart(self.cart)
        messages.add_message(request, messages.INFO, "Кол-во успешно изменено")
        return HttpResponseRedirect('/shop/cart/')


class CheckoutView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.all()
        form = OrderForm(request.POST or None)
        context = {
            'cart': self.cart,
            'categories': categories,
            'form': form
        }
        return render(request, 'checkout.html', context)


class CartView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.all()
        context = {
            'cart': self.cart,
            'categories': categories
        }
        return render(request, 'cart.html', context)


class MakeOrderView(CartMixin, View):

    @transaction.atomic
    @csrf_exempt
    def post(self, request, *args, **kwargs):

        form = OrderForm(request.POST or None)
        customer = self.cart.owner
        if form.is_valid():
            new_order = form.save(commit=False)
            new_order.customer = customer
            new_order.first_name = form.cleaned_data['first_name']
            new_order.last_name = form.cleaned_data['last_name']
            new_order.phone = form.cleaned_data['phone']
            new_order.address = form.cleaned_data['address']
            new_order.buying_type = form.cleaned_data['buying_type']
            # new_order.order_date = form.cleaned_data['order_date']
            new_order.comment = form.cleaned_data['comment']
            new_order.save()
            self.cart.in_order = True
            self.cart.save()
            new_order.cart = self.cart
            new_order.save()
            customer.orders.add(new_order)
            messages.add_message(request, messages.INFO, 'Спасибо за заказ! Менеджер с Вами свяжется')
            return HttpResponseRedirect('/shop/')
        return HttpResponseRedirect('/shop/checkout/')
