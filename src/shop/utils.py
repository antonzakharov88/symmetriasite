import time
from datetime import date

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from shop.models import Order, Product, Cart, CartProduct


def recalc_cart(cart):
    cart_data = cart.products.aggregate(models.Sum('final_price'), models.Count('id'))
    if cart_data.get('final_price__sum'):
        cart.final_price = cart_data['final_price__sum']
    else:
        cart.final_price = 0
    cart.total_products = cart_data['id__count']
    cart.save()




# def recalc_product():
#     order_list = Order.objects.filter(order_date=date.today())
#     for order in order_list:
#         for product in order.cart.products.product.all():
#             product.count -= order.cart.products.qte
#             return product.count




