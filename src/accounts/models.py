from django.contrib.auth import password_validation
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from django.utils.translation import gettext_lazy as _


# Create your models here.


class User(AbstractUser):

    class BUYER_STATUS(models.IntegerChoices):
        NEW = 0, "New"
        EXPERIENCED = 1, "experienced"
        REGULAR = 2, "regular"
        VIP = 3, "VIP"

    username_validator = UnicodeUsernameValidator()   
     
    username = models.CharField(
        _('Логин'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("Аккаунт с таким логином уже существует"),
        },
    )
    
    first_name = models.CharField(_('имя'), max_length=150, blank=False)
    last_name = models.CharField(_('фамилия'), max_length=150, blank=False)
    phone_number = PhoneNumberField(_('номер телефона'), region="UA")
    location = models.CharField(_('город'), max_length=30, blank=True)
    birth_date = models.DateField(_('дата рождения'), null=True, blank=True)
    image = models.ImageField(_('аватар'), null=True, default='IMG_3652.JPG', upload_to='avatars/')
    status = models.PositiveIntegerField(choices=BUYER_STATUS.choices, default=BUYER_STATUS.NEW)
    orders = models.ManyToManyField(to='shop.Order', verbose_name='Заказы покупателя', related_name='related_order')


    def save(self, *args, **kwargs):

        super().save(*args, **kwargs)
        if self._password is not None:
            password_validation.password_changed(self._password, self)
            self._password = None