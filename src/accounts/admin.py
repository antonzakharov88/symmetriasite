from django.contrib import admin # noqa

# Register your models here.
from accounts.models import User

admin.site.register(User)
