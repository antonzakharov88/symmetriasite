from django.contrib import messages
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm, LoginForm
from accounts.models import User
from shop.mixins import CartMixin
from shop.models import Order, Category, Cart


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully registered')
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('shop:mainpage')
    template_name = 'login.html'
    form_class = LoginForm

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('shop:mainpage')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'{self.request.user} вы вошли в аккаунт')
        return result


class AccountUpdateView(CartMixin, UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('accounts:profile')
    form_class = AccountUpdateForm


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer = self.get_object()
        context['customer'] = customer
        context['orders'] = Order.objects.filter(customer=customer).order_by("-id")
        context['categories'] = Category.objects.all()
        return context

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f" Ваш профиль: {self.request.user} был успешно обновлен")
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.info(self.request, f'User {self.request.user} has been logged out')
        return super().dispatch(request, *args, **kwargs)
