from celery import shared_task
import requests
from django.db.models.signals import pre_save
from django.dispatch import receiver

from accounts.models import User


@receiver(pre_save, sender=User)
def check_new_user(sender, instance, created, **kwargs):
    if created:
        username = instance.username
        text = f'Новый пользователь {username}'
        send_telegram.delay(text)



@shared_task
def send_telegram(text):
    token = "1880174617:AAGX0KXxJZXWj4xZWmyrhT6rx_tj-_q48e0"
    url = "https://api.telegram.org/bot"
    channel_id = "463377689"
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text
          })

    if r.status_code != 200:
        raise Exception("post_text error")