from django.contrib import admin # noqa
from django.urls import path

from accounts.views import AccountRegistrationView, AccountLoginView, AccountUpdateView, AccountLogoutView

app_name = 'accounts'
urlpatterns = [

    path('registration/', AccountRegistrationView.as_view(), name='registration'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('profile/', AccountUpdateView.as_view(), name='profile'),
    # path('password/', AccountPasswordChangeView.as_view(), name='password'),
    # path('delete/', AccountDeleteView.as_view(), name='profile_delete'),
    # path('leaders/', UsersListView.as_view(), name='leaders'),
    # path('contact_us/', ContactUsView.as_view(), name='contact_us')

]