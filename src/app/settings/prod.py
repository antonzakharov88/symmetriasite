
from app.settings.base import * # noqa


DEBUG = False

MEDIA_ROOT = '/var/www/symmetriasite/media'
STATIC_ROOT = '/var/www/symmetriasite/static'
