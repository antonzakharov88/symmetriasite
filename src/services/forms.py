from services.models import PermanentMakeUp
import django_filters


class PermanentFilter(django_filters.FilterSet):
    class Meta:
        model = PermanentMakeUp
        # fields = ['first_name', 'last_name', 'age']
        fields = {
            'price': ['lt', 'gt'],
            'name': ['exact', 'icontains'],
        }
