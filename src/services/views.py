from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import DetailView, View

from services.forms import PermanentFilter
from services.models import PermanentMakeUp


class BaseServiceView(View):

    def get(self, request, *args, **kwargs):
        # categories = Category.objects.all()
        services = PermanentMakeUp.objects.all
        context = {
            # 'categories': categories,
            'services': services,
            # 'cart': self.cart
        }
        return render(request, 'services.html', context)


class ServiceDetailView(DetailView):
    model = PermanentMakeUp
    context_object_name = 'service'
    template_name = 'service_detail.html'
    slug_url_kwarg = 'slug'

    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        return self.model.objects.get(slug=slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['categories'] = Category.objects.all()
        # context['cart'] = self.cart
        return context
