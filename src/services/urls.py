from django.contrib import admin  # noqa
from django.urls import path
# from django.views.generic import TemplateView

from services.views import *

app_name = 'services'
urlpatterns = [

    path('', BaseServiceView.as_view(), name='base_services'),
    path('services/<str:slug>/', ServiceDetailView.as_view(), name='service_detail'),

]
