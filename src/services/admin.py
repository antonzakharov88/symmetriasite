from django.contrib import admin

# Register your models here.
from services.models import PermanentMakeUp

admin.site.register(PermanentMakeUp)
