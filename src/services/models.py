from django.db import models
from django.urls import reverse

from django.utils.translation import gettext_lazy as _
from PIL import Image
from decimal import *


class PermanentMakeUp(models.Model):
    name = models.CharField(_('Название'), max_length=150, blank=False)
    slug = models.SlugField(unique=True)
    description = models.TextField(_('описание'), blank=True)
    image = models.ImageField(_('изображение'), null=True, default='PM.JPG', upload_to='covers/')
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Цена', null=True)
    discount = models.IntegerField(verbose_name="Скидка", default=0)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Стоимость со скидкой', default=0)

    def _get_final_price(self):
        if self.discount:
            self.final_price = Decimal(self.price - (self.price / 100) * self.discount).quantize(Decimal('1.11'),
                                                                                                 rounding=ROUND_UP)
        else:
            self.final_price = self.price
        return self.final_price

    def get_absolute_url(self):
        return reverse('services:service_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.name}'

    def save(self, *args, **kwargs):
        self.final_price = self._get_final_price()
        super(PermanentMakeUp, self).save(*args, **kwargs)

        if self.image:
            img = Image.open(self.image.path)

            if img.height > 600 or img.width > 600:
                output_size = (600, 600)
                img.thumbnail(output_size)
                img.save(self.image.path)
